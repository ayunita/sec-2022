import hashlib
import time

def is_guess_correct(key):
    try:
        x = results[key]
        return key
    except:
        return ""

def update_results(guess, word, guess_count):
    results[guess]["word"] = word
    results[guess]["guess"] = guess_count
    results[guess]["time"] = time.time() - start_time
    
# Original wordlist
def run_og_wordlist(wordlist):
    guess_count = 0
    symbols = ["~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=", "{", "[", "}", "]", "|", "\\", "\"", ":", ";", "'", "<", ",", ">", ".", "?", "/"]
    
    for word in wordlist:
        # original word
        guess_count += 1
        guess(word, guess_count)
        
        # flc = first letter capitalized (e.g. Password)
        guess_count += 1
        flc = word.capitalize()
        guess(flc, guess_count)

        # llc = last letter capitalized (e.g. passworD)
        guess_count += 1
        llc = word[:-1] + word[-1].upper()
        guess(llc, guess_count)

        # fllc = first & last letter capitalized (e.g. PassworD)
        guess_count += 1
        fllc = flc[:-1] + flc[-1].upper()
        guess(fllc, guess_count)

        # Adds symbol to word
        for symbol in symbols:
            guess_count += 1
            ws = word + str(symbol)
            guess(ws, guess_count) # hello!

            guess_count += 1
            sw = str(symbol) + word
            guess(sw, guess_count) # !hello

            guess_count += 1
            flcs = flc + str(symbol)
            guess(flcs, guess_count) # Hello!

            guess_count += 1
            sflc = str(symbol) + flc
            guess(sflc, guess_count) # !Hello

            guess_count += 1
            llcs = llc + str(symbol)
            guess(llcs, guess_count) # hellO!

            guess_count += 1
            sllc = str(symbol) + llc
            guess(sllc, guess_count) # !hellO

            guess_count += 1
            fllcs = fllc + str(symbol)
            guess(fllcs, guess_count) # HellO!

            guess_count += 1
            sfllc = str(symbol) + fllc
            guess(sfllc, guess_count) # !HellO

        # Adds year to word
        for year in range(1990, 2023):
            guess_count += 1
            wy = word + str(year)
            guess(wy, guess_count) # hello1999

            guess_count += 1
            yw = str(year) + word
            guess(yw, guess_count) # 1999hello

            guess_count += 1
            flcy = flc + str(year)
            guess(flcy, guess_count) # Hello1999

            guess_count += 1
            yflc = str(year) + flc
            guess(yflc, guess_count) # 1999Hello

            guess_count += 1
            llcy = llc + str(year)
            guess(llcy, guess_count) # hellO1999

            guess_count += 1
            yllc = str(year) + llc
            guess(yllc, guess_count) # 1999hellO

            guess_count += 1
            fllcy = fllc + str(year)
            guess(fllcy, guess_count) # HellO1999

            guess_count += 1
            yfllc = str(year) + fllc
            guess(yfllc, guess_count) # 1999HellO

            # Adds symbol to word and year
            # s = symbol, wys -> word + year + symbol, swy -> symbol + word + year
            for symbol in symbols:
                guess_count += 1
                wys = wy + str(symbol)
                guess(wys, guess_count) # hello2000!

                guess_count += 1
                swy = str(symbol) + wy
                guess(swy, guess_count) # !hello2000

                guess_count += 1
                wsy = word + str(symbol) + str(year)
                guess(wsy, guess_count) # hello!2000

                guess_count += 1
                yws = str(year) + word + str(symbol)
                guess(yws, guess_count) # 2000hello!

                guess_count += 1
                syw = str(symbol) + yw
                guess(syw, guess_count) # !2000hello

                guess_count += 1
                ysw = str(year) + str(symbol) + word
                guess(ysw, guess_count) # 2000!hello

                # FLC
                guess_count += 1
                flcys = flcy + str(symbol)
                guess(flcys, guess_count)

                guess_count += 1
                sflcy = str(symbol) + flcy
                guess(sflcy, guess_count)

                guess_count += 1
                flcsy = flc + str(symbol) + str(year)
                guess(flcsy, guess_count)

                guess_count += 1
                yflcs = str(year) + flc +  str(symbol)
                guess(yflcs, guess_count) 

                guess_count += 1
                syflc = str(symbol) + yflc
                guess(syflc, guess_count)

                guess_count += 1
                ysflc = str(year) +  str(symbol) + flc
                guess(ysflc, guess_count)

                # LLC
                guess_count += 1
                llcys = llcy + str(symbol)
                guess(llcys, guess_count)

                guess_count += 1
                sllcy = str(symbol) + llcy
                guess(sllcy, guess_count)

                guess_count += 1
                llcsy = llc +  str(symbol) + str(year)
                guess(llcsy, guess_count)

                guess_count += 1
                yllcs = str(year) + llc +  str(symbol)
                guess(yllcs, guess_count)

                guess_count += 1
                syllc = str(symbol) + yllc
                guess(syllc, guess_count)

                guess_count += 1
                ysllc = str(year) + str(symbol) + llc
                guess(ysllc, guess_count)

                # FLLC
                guess_count += 1
                fllcys = fllcy + str(symbol)
                guess(fllcys, guess_count)

                guess_count += 1
                sfllcy = str(symbol) + fllcy
                guess(sfllcy, guess_count)

                guess_count += 1
                fllcsy = fllc +  str(symbol) + str(year)
                guess(fllcsy, guess_count)

                guess_count += 1
                yfllcs = str(year) + fllc +  str(symbol)
                guess(yfllcs, guess_count)

                guess_count += 1
                syfllc = str(symbol) + yfllc
                guess(syfllc, guess_count)

                guess_count += 1
                ysfllc = str(year) + str(symbol) + fllc
                guess(ysfllc, guess_count)

    return guess_count

def guess(string, guess_count):
    guess = hashlib.sha1(string.encode("utf8")).hexdigest()
    if is_guess_correct(guess):
        update_results(guess, string, guess_count)
        print(guess + ": " + str(results[guess]))

def crack(wordlist):
    global start_time
    start_time = time.time()

    guess_count = run_og_wordlist(wordlist)

    print(guess_count)
try:
    list1 = []
    list2 = []
    wordlist1 = open("wordlist1.txt", "r")
    wordlist2 = open("wordlist2.txt", "r")
    for w in wordlist1:
        list1.append(w.rstrip())
    for w in wordlist2:
        list2.append(w.rstrip())

    wordlist = list(set().union(list1, list2))

    global results
    results = {}

    passwords1 = open("basic8survey", "r")
    passwords2 = open("dictionary8", "r")
    passwords3 = open("comprehensive8", "r")
    passwords4 = open("basic16", "r")
    for p in passwords1:
        password = p.rstrip()
        results[password] = { "type": "basic8survey", "word": "", "guess": 0, "time": 0}
    for p in passwords2:
        password = p.rstrip()
        results[password] = { "type": "dictionary8", "word": "", "guess": 0, "time": 0}
    for p in passwords3:
        password = p.rstrip()
        results[password] = { "type": "comprehensive8", "word": "", "guess": 0, "time": 0}
    for p in passwords4:
        password = p.rstrip()
        results[password] = { "type": "basic16", "word": "", "guess": 0, "time": 0}

    # wordlist = ["fossicked"]

    crack(wordlist)

    # print(json.dumps(results, indent=2))

except Exception as e:
    print(e)
finally:
    wordlist1.close()
    wordlist2.close()
    passwords1.close()
    passwords2.close()
    passwords3.close()
    passwords4.close()
